# Build image
FROM node:12.22-alpine AS build

ARG BASE_PATH
ARG DATABASE_TYPE
ARG UMAMI_VERSION=v1.31.0

ENV BASE_PATH=$BASE_PATH
ENV DATABASE_URL "postgresql://umami:umami@db:5432/umami"
ENV DATABASE_TYPE=$DATABASE_TYPE

RUN apk add git

RUN git clone https://github.com/mikecao/umami.git /build
WORKDIR /build
RUN git checkout ${UMAMI_VERSION}

#RUN git log -n 1 --pretty=format:'%h'
#RUN git log -n 1 --pretty=format:'%H'
#RUN git log -n 1 --pretty=format:'%H(MISSING)'

RUN yarn config set --home enableTelemetry 0

RUN ls -la
RUN pwd

# No need, files are already  here
# COPY package.json yarn.lock /build/

# Install only the production dependencies
RUN yarn install --production --frozen-lockfile

# Cache these modules for production
RUN cp -R node_modules/ prod_node_modules/

# Install development dependencies
RUN yarn install --frozen-lockfile

# No need, files are already here
# COPY . /build

RUN yarn next telemetry disable
RUN yarn build

# Production image
FROM node:12.22-alpine AS production
WORKDIR /app

# Copy cached dependencies
COPY --from=build /build/prod_node_modules ./node_modules

# Copy generated Prisma client
COPY --from=build /build/node_modules/.prisma/ ./node_modules/.prisma/

COPY --from=build /build/yarn.lock /build/package.json ./
COPY --from=build /build/.next ./.next
COPY --from=build /build/public ./public

USER node

EXPOSE 3000
CMD ["yarn", "start"]
